package com.mevg.lalo.eva1_5_archivos_sdcard;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class MainActivity extends AppCompatActivity {
    TextView txvMostrar;
    String pathSDCARD;
    EditText txtEscirbir;
    Button btnGuardar, btnAbrir;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        pathSDCARD = Environment.getExternalStorageDirectory().getAbsolutePath();
        Toast.makeText(MainActivity.this, pathSDCARD, Toast.LENGTH_LONG).show();
        //leer el archivo
        txvMostrar = (TextView) findViewById(R.id.mostrar);
        txtEscirbir = (EditText) findViewById(R.id.escribir);
        btnAbrir = (Button) findViewById(R.id.leer);
        btnGuardar = (Button) findViewById(R.id.guardar);
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE ) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    1000);
        }
        else{
            readFile();
        }
        btnAbrir.setOnClickListener((v) -> {
            readFile();
        });

        btnGuardar.setOnClickListener((v) -> {
            writeFile();
        });

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode == 1000){
            Toast.makeText(MainActivity.this, permissions[0] + " " + grantResults[0], Toast.LENGTH_LONG).show();
            Toast.makeText(MainActivity.this, permissions[1] + " " + grantResults[1], Toast.LENGTH_LONG).show();
            readFile();
        }
    }

    private void readFile(){
        try{
            FileInputStream fis = new FileInputStream(pathSDCARD +  "/data.txt");
            InputStreamReader isr = new InputStreamReader(fis);
            BufferedReader reader = new BufferedReader(isr);
            String text;
            while((text = reader.readLine()) != null){
                txvMostrar.append(text + "\n");
            }
            reader.close();

            fis.close();
        }catch(FileNotFoundException e){
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void writeFile(){
        try {
            FileOutputStream fos = new FileOutputStream(pathSDCARD + "/data.txt");
            OutputStreamWriter osw = new OutputStreamWriter(fos);
            BufferedWriter writer = new BufferedWriter(osw);
            String text = txtEscirbir.getText().toString();
            txvMostrar.append("\n" + text);
            writer.write(text + "\n");
            fos.close();
            writer.close();
        }catch (FileNotFoundException e){
            e.printStackTrace();
        } catch (IOException e){
            e.printStackTrace();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        //Agregar contenido al archivo de texto
    }
}
